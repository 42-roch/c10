/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 01:55:58 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 14:51:17 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"

void	print_file(char **argv, char *name)
{
	int		r;

	r = ft_read_file(argv, name);
	if (r == -1)
		return ;
	close(r);
}

void	read_standard_input(void)
{
	char	ch;

	while (read(STDIN_FILENO, &ch, 1) > 0)
		ft_putchar(ch);
}

int	main(int argc, char **argv)
{
	int	i;

	if (argc == 1)
	{
		read_standard_input();
		return (0);
	}
	i = 1;
	while (argv[i])
	{
		print_file(argv, argv[i]);
		i++;
	}
}
