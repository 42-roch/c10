/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 03:10:46 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 16:08:48 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"
#include <libgen.h>
#include <string.h>
#include <errno.h>

int	ft_read_file(char **argv, char *name)
{
	char	buffer[1024];
	int		r;
	int		ret;

	r = open(name, O_RDONLY);
	if (r == -1)
	{
		print_error(argv, name);
		return (r);
	}
	ret = read(r, &buffer, 1024);
	while (ret > 0)
	{
		write(1, buffer, ret);
		ret = read(r, &buffer, 1024);
	}
	return (r);
}
