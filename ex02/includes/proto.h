/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   proto.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 01:55:26 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 19:29:42 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROTO_H
# define PROTO_H

# include <fcntl.h>
# include <unistd.h>
# include <libgen.h>
# include <string.h>
# include <errno.h>

void			ft_putchar(char c);
void			ft_putstr(char *str);
int				ft_read_file(char **argv, char *name, unsigned int size);
int				contains_options(char **a);
unsigned int	count_chars(char *file);
int				print_error(char **argv, char *file);
void			print_header(int files, int options, int file, char *name);
int				ft_atoi(char *str);
int				get_option(char **argv);
int				print_error_(char **argv, char *msg, char *msg1);

#endif