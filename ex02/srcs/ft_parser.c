/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 13:06:35 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 17:45:56 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"

int	ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
		i++;
	return (s1[i] - s2[i]);
}

int	contains_options(char **a)
{
	int	b;

	b = 0;
	while (a[b])
	{
		if (ft_strcmp(a[b], "-c") == 0)
			return (1);
		b++;
	}
	return (0);
}

int	get_option(char	**argv)
{
	int	a;

	a = 0;
	while (argv[a])
	{
		if (ft_strcmp(argv[a], "-c") == 0)
			return (ft_atoi(argv[a + 1]));
		a++;
	}
	return (-2);
}
