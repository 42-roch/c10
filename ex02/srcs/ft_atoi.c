/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 12:06:16 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 19:32:40 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	is_number(char a)
{
	if (a >= '0' && a <= '9')
		return (0);
	return (1);
}

int	ft_atoi(char *str)
{
	int	i;
	int	target;

	target = 0;
	i = 0;
	while (str[i])
	{
		if (str[i] == '\0'
			|| (str[i] >= 9 && str[i] <= 13)
			|| str[i] == 32
			|| is_number(str[i]) == 1)
		{
			i++;
			return (-1);
		}
		target = target * 10 + (str[i] - '0');
		i++;
	}
	return (target);
}
