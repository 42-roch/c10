/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 13:04:06 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 19:34:14 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"
#include<stdlib.h>

void	print_file(char **argv, char *file, unsigned int size)
{
	int		r;

	r = ft_read_file(argv, file, size);
	if (r == -1)
		return ;
	close(r);
}

void	print_files(int argc, char **argv, char **files, unsigned int size)
{
	int	i;

	i = 0;
	while (files[i])
	{
		print_header(argc, contains_options(argv), i, files[i]);
		print_file(argv, files[i], size);
		i++;
	}
}

int	main(int argc, char **argv)
{
	int	option;

	if (argc <= 1)
		return (0);
	else if (argc > 1 && !contains_options(argv))
	{
		print_files(argc, argv, &argv[1], 10);
		return (0);
	}
	if (argc <= 2)
		return (print_error_(argv, "option requires an argument -- ", "c"));
	option = get_option(argv);
	if (option == -1)
		return (print_error_(argv, "illegal offset -- ", argv[2]));
	print_files(argc, argv, &argv[3], option);
	return (0);
}
