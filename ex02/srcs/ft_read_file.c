/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 03:10:46 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 20:01:17 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"
#include<stdio.h>

int	count_new_lines(char buffer[])
{
	int	a;
	int	l;

	a = 0;
	l = 0;
	while (buffer[a])
	{
		if (buffer[a] == '\n')
			l++;
		a++;
	}
	return (l);
}

void	print(int r, int c, unsigned int size)
{
	int				ret;
	unsigned int	chars;
	char			buffer[40960];

	ret = read(r, &buffer, 40960);
	chars = 0;
	while (ret > 0)
	{
		while (buffer[chars])
		{
			if (c - size <= chars && (int) chars < c)
				write(1, &buffer[chars], 1);
			chars++;
		}
		ret = read(r, &buffer, 40960);
	}
	close(r);
}

int	ft_read_file(char **argv, char *name, unsigned int size)
{
	int		r;
	int		c;

	c = count_chars(name);
	if (c == -1)
	{
		print_error(argv, name);
		return (-1);
	}
	r = 0;
	r = open(name, O_RDONLY);
	print(r, c, size);
	return (r);
}

void	print_header(int files, int options, int file, char *name)
{
	int	l;

	files = files - 1;
	if (options == 1)
		files = files - 2;
	l = count_chars(name);
	if (files <= 1 || l == -1)
		return ;
	if (file >= 1)
		ft_putstr("\n");
	ft_putstr("==> ");
	ft_putstr(basename(name));
	ft_putstr(" <==\n");
}
