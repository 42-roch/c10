/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 14:43:05 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 19:29:31 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"

int	print_error(char **argv, char *file)
{
	ft_putstr(basename(argv[0]));
	ft_putstr(": ");
	ft_putstr(file);
	ft_putstr(": ");
	ft_putstr(strerror(errno));
	ft_putstr("\n");
	return (0);
}

int	print_error_(char **argv, char *msg, char *msg1)
{
	ft_putstr(basename(argv[0]));
	ft_putstr(": ");
	ft_putstr(msg);
	ft_putstr(msg1);
	ft_putstr("\n");
	return (0);
}
