/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_chars.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 14:21:39 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 19:59:37 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>

int	count_chars(char *file)
{
	int				r;
	char			buffer[40960];
	int				chars;
	int				ret;

	r = open(file, O_RDONLY);
	if (r == -1)
		return (r);
	chars = 0;
	ret = read(r, &buffer, 40960);
	while (ret > 0)
	{
		chars += ret;
		ret = read(r, &buffer, 40960);
	}
	close(r);
	return (chars);
}
