/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   messages.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/14 22:14:33 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/15 01:14:01 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MESSAGES_H
# define MESSAGES_H

# define FILE_NAME_MISSING "File name missing.\n"
# define TOO_MANY_ARGS "Too many arguments.\n"
# define CANNOT_READ_FILE	"Cannot read file.\n"

#endif