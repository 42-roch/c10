/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/14 11:21:51 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 19:20:28 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "../includes/proto.h"
#include "../includes/messages.h"

int	validate(int c, char **args)
{
	int	r;

	if (c < 2)
	{
		ft_putstr(FILE_NAME_MISSING);
		return (-1);
	}
	else if (c >= 3)
	{
		ft_putstr(TOO_MANY_ARGS);
		return (-1);
	}
	r = open(args[1], O_RDONLY);
	if (r == -1)
		ft_putstr(CANNOT_READ_FILE);
	return (r);
}

int	main(int argc, char **argv)
{
	int		r;
	char	buffer[1024];
	int		ret;

	r = validate(argc, argv);
	if (r == -1)
		return (r);
	ret = read(r, &buffer, 1024);
	while (ret > 0)
	{
		write(1, &buffer, ret);
		ret = read(r, &buffer, 1024);
	}
	close(r);
	return (0);
}
